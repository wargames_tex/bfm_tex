from wgexport import *

moreInfo = '''
<html>
 <head>
  <title>{title}</title>
 </head>
 <body>
  <h1>{title}</h1>
  <h2>Initial deployment</h2>
  <p>
    The Soviet units have been deployed in a semi-historical way.  You
    can of course change it, though it will have no effect on the
    game-play as all Soviet units are the same at the onset.
  </p>
  <p>

    You can set up the German units semi-historically by selecting the
    German unit stack and then select <b>Deploy semi-historic</b> from
    the context menu.
  </p>
  <p>
    You can ofcourse change the set-up if you like.  Note the letter
    in the top-left corner (<i>N</i>, <i>M</i>, <i>S</i>) indicate the
    historical set-up region of the front (North, Middle, South).
  <h2>Turn track</h2>
  <p>
    To advance between phases, use the turn track interface in the
    tool bar. This will automatically progress the turn marker on
    the turn track, and will calculate properties derived from the
    turn (mud, snow, etc.).
  </p>
  <p>
   Use the short cut Alt-T to move on to the next phase.  This will
   also take care of some booking for you, for example, weather.
  </p>
  <h2>Reminders</h2>
  <p>
   The module will remind the faction about available replacements, as
   well as limitations due to the ground conditions (Half CF and MF=1
   on mud, -1 MF for infantry and -2 MF for armoured during snow).   
  </p>
  <h2>Combat declaration</h2>
  <p>
   To declare a combat in the combat phases,
  </p>
  <ol>
   <li>Select the attackers <i>and</i> defender. Use
     for example Cltr-Mouse-1 to select multiple units. Remember
     to deselect any units (click on map) before starting a new
     combat declaration.</li>
   <li>Press the Odds button (?:1 or Ctrl-Y) in the menu bar.
     <ul>
      <li>This will add a unique combat marker to each
        combatant (up to 12 such unique markers),</li>
      <li>Calculate the odds and place an odds marker on the
        defending unit.  Note that calculations are shown in
        the chat window</li>
     </ul>
     If you regret a declaration, simple right-click (Mouse-2)
     the combat markers and select <i>Cancel</i> from the pop-up
     menu, or press Ctrl-C</li>
   <li>Continue this until no more combats need to be declared.</li>
  </ol>
  <p>
    Note that the module <i>does not</i> prevent you from declaring
    illegal battles (non-adjacent units, multiple attacks, f.ex.).
    You can your opponent should be vigilant about this.
  </p>
  <p>
   The module will <i>not</i> allow declaration of a battle at
   odds 1:3 or worse.  You will get  battle markers, but no odds
   marker.  Simply clear the battle markers. 
  </p>
  <h2>Combat resolution</h2>
  <p>
   Once all combats have been declared, you can resolve them in
   any order you like.  To resolve a combat
  </p>
  <ol>
   <li>Right click (Mouse-2) the odds marker, and select
     <i>Resolve</i> from the pop-up menu, or press Ctrl-X</li>
   <li>This will
    <ol>
     <li>Roll the dice (result shown in chat)</li>
     <li>Determine the combat result (shown in chat)</li>
     <li>Place a combat result marker on the defender</li>
    </ol></li>
   <li>Now you need to implement the combat results. You and your
    opponent must retreat, take step losses, eliminate units, and
    advance, as required.  If you play by mail, then this is typically
    done partially by sending your log file to your opponent, who
    then implements the relevant parts.
   </li>
   <li>If you want, you can now clear the combat by right clicking
    the result marker and selecting <i>Clear</i>.  Alternatively,
    you can leave the markers - they will be cleared at the end
    of the combat phase.</li>
  </ol>
  <h2>Step-losses and elimination</h2>
  <p>
    To inflict a step loss on a unit, select that unit and select
    <b>Step loss</b> from the context menu, or press
    <code>Ctrl-F</code>.  The unit will be flipped to it's reduced
    size.  If the unit is already in a reduced state, then this
    eliminates the unit.
  </p>
  <p>
    Units can also be directly eliminated by selecting the unit and
    then selecting <b>Eliminate</b> from the context menu or press
    <code>Ctrl-E</code>.
  </p>
  <h2>Replacements and reinforcements</h2>
  <p>
    The module will tell the current faction how many replacement
    steps are available at the start of each replacement phase.
  </p>
  <p>
    The module <i>does</i> keep track of how many steps where used,
    and will refuse to do more replacements or reinforcements when the
    replacement points have been used up.  If you regret a replacement
    or reinforcement, you can either undo (<code>Ctrl-Z</code>) the
    move, or for replacements, put the unit back in the pool, or for
    reinforcement, inflict a step loss (<code>Ctrl-F</code>) on the
    newly reinforced unit.
  </p>
  <p>
    Note that the module <i>does not</li> prevent you from building
    a new replacement full-strenght unit, nor does it prevent you
    from removing old units and regain replacement points in that way.
    You and your opponent should be vigilant about this.
  </p>
  <p>
    New, half-strength replacement units are built by dragging them from the
    factions pool on to the map.
  </p>
  <p>
    New Soviet units arrive on the east edge of the map (and
    <i>only</i> the east edge), or in any city, in communication with
    the east dge, and controlled by the Soviet faction.
  </p>
  <p>
    New German units arrive on the west edge of the map, or in any
    city , in communication with the west dge, and controlled by the
    German faction.
  </p>
  <p>
    A unit can be reinforced by selecting that unit and then selecting
    <b>Reinforce</b> from the context menu, or by pressing
    <code>Ctrl-R</code>.  To be reinforced, the unit must be in
    communication with its faction map edge.  The Soviet faction may,
    however, reinforce a unit in the Moscow hex, whether Moscow is in
    communication with the east edge or not.  <i>No other</i> city has
    this capability, and it does not apply to German units occupying
    Moscow.
  </p>
  <p>
    In-communication status is <i>not</i> checked by the module.
  </p>
  <h2>Changelog</h2>
  <dl>
    <dt>1.0</dt>
    <dd>Initial release</dd>
    <dt>1.1</dt>
    <dd>Various small fixes</dd>
    <dt>1.2</dt>
    <dd>
      <ul>
       <li>Symbolic dice</li>
       <li>"Flipping" (<code>Ctrl-F</code>) now eliminates if unit
         has already suffered a step loss</li>
       <li>Eliminated (<code>Ctrl-E</code>) units are automatically
         flipped to their reduced side.</li>
       <li>Command to deploy German units semi-historic</li>
       <li>Soviet initial deployment is semi-historic.</li>
       <li>Keep track of replacement points and disallow
         illegal replacements or reinforcements.</li>
       <li>Step loss and elimination limited to the <b>combat</b>
         phases.</li>
     </ul>
    </dd>
  </dl>
  <h2>About this module</h2>
  <p>
   This VASSAL module was created from
   L<sup>A</sup>T<sub>E</sub>X sources of a Print'n'Play version
   of the <i>Battle for Moscow</i> game.  That (a PDF) can be
   found at
  </p>
  <center>
   <code>https://gitlab.com/wargames_tex/bfm_tex</code></a>
  </center>
  <p>
   where this module can also be found.
  </p>
  <p>
   The original game was developed by Frank Chadwick and is available from
  </p>
  <center><code>http://grognard.com/bfm</code></center>
  <p>
   The game was prepared using the L<sup>A</sup>T<sub>E</sub>X
   package <b>wargame</b> available from
  </p>
  <center>
   <code>https://gitlab.com/wargames_tex/wargme_tex</code>
  </center>
  <p>
   This work is &#127279; 2022 Christian Holm Christensen, and
   licensed under the Creative Commons Attribution-ShareAlike 4.0
   International License. To view a copy of this license, visit
  </p>
  <center>
   <code>http://creativecommons.org/licenses/by-sa/4.0</code>
  </center>
  <p>
   or send a letter to
  </p>
  <center>
   Creative Commons<br>
   PO Box 1866<br>
   Mountain View<br>
   CA 94042<br>
   USA
  </center>
 </body>
</html>'''
# --------------------------------------------------------------------
# Our main patching function
def patch(build,data,vmod,verbose=False):

    fileMapping    = vmod.getFileMapping()
    def getImage(name,mapping=fileMapping):
        from pathlib import Path
        
        real = mapping.get(Path(name).stem,None)
        if real is None:
            print(f'File {name} not found!')
            return None

        return Path(real).name
    
    # --- Global properties ------------------------------------------
    debug          = 'wgDebug'
    hidden         = 'wg hidden unit'
    battleUnit     = 'wgBattleUnit'
    battleCalc     = 'wgBattleCalc'
    battleCtrl     = 'wgBattleCtlr'
    battleNo       = 'wgBattleNo'
    battleShift    = 'wgBattleShift'
    currentBattle  = 'wgCurrentBattle'
    oddsProto      = 'OddsMarkers prototype'
    globalFortress = 'FortressHexes'
    globalForest   = 'ForestHexes'
    globalCross    = 'RiverEdges'
    globalHex      = 'DefenderHex'
    globalEdge     = 'RiverEdge'
    globalCombat   = 'NotCombat'
    globalWeather  = 'Weather'
    germanRP       = 'GermanRP'
    sovietRP       = 'SovietRP'
    
    # --- Internal keys ----------------------------------------------
    updateHex      = key(NONE,0)+',updateHex'
    updateEdge     = key(NONE,0)+',updateEdge'
    resetEdge      = key(NONE,0)+',resetEdge'
    checkCombat    = key(NONE,0)+',checkCombat'
    checkGermanRP  = key(NONE,0)+',checkGermanRP'
    checkSovietRP  = key(NONE,0)+',checkSovietRP'
    advanceTurn    = key(NONE,0)+',advanceTurn'
    specialAvail   = key(NONE,0)+',specialAvail'
    tankAvail      = key(NONE,0)+',tankAvail'
    nextFaction    = key(NONE,0)+',nextFaction'
    resolveKey     = key('Y')
    setBattle      = key(NONE,0)+',wgSetBattle'
    dieRoll        = key(NONE,0)+',dieRoll'
    deployKey      = key(NONE,0)+',deployHistory'
    stepKey        = key(NONE,0)+',stepLoss'
    elimKey        = key(NONE,0)+',eliminate'
    nextPhase      = key('T',ALT)
    reinforceKey   = key('R')
    reinforceBase  = key(NONE,0)+',reinforce'
    reinforceReal  = reinforceBase+'Real'
    reinforceDecr  = reinforceBase+'Decr'
    reinforceIncr  = reinforceBase+'Incr'
    reinforceMove  = reinforceBase+'Move'
    

    # --- Names of phases --------------------------------------------
    phaseNames     = ['German replacement',
                      'German panzer movement',
                      'German combat',
                      'German movement',
                      'Soviet replacement',
                      'Soviet rail movement',
                      'Soviet combat',
                      'Soviet movement']
    germanCombat   = phaseNames[2]
    sovietCombat   = phaseNames[-2]
    germanSpecial  = phaseNames[1]
    germanMove     = phaseNames[3]
    sovietMove     = phaseNames[-1]
    sovietRepl     = phaseNames[4]
    germanRepl     = phaseNames[0]
    # --- Information about hexes ------------------------------------
    hexsep         = '@'
    shex           = lambda h : h[:2]+hexsep+h[2:] # h.lstrip('0')
    shexes         = lambda hx : [shex(h) for h in hx]
    forestHexes    = ['0201', '0202', '0207','0208',
                      '0301', '0308',
                      '0401', '0405', '0408','0409',
                      '0510',
                      '0604',
                      '0701', '0702', '0703', '0704', '0708',
                      '0806',
                      '0901', '0902', '0905','0906',
                      '1201',
                      '1303',
                      '1404']
    forestStr      = ':'+':'.join(shexes(forestHexes))+':'
    fortressHexes  = ([f'05{r:02d}' for r in range(1,8)]+
                      [f'08{r:02d}' for r in range(1,6)]+
                      [f'10{r:02d}' for r in range(1,5)]+
                      [f'11{r:02d}' for r in range(2,8) if r != 3]+
                      ['1203'])
    fortressStr    = ':'+':'.join(shexes(fortressHexes))+':'
    # S, SE, NE edges, we will invert search later on
    overRiver      = [["0105","0106"],["0105","0205"],
                      ["0204","0205"],["0204","0305"],
                      ["0305","0306"],["0305","0405"],
                      ["0403","0504"],
                      ["0404","0405"],["0404","0505"],["0404","0504"],
                      ["0503","0504"],["0503","0603"],["0504","0604"],
                      ["0505","0506"],["0505","0605"],["0505","0604"],
                      ["0603","0604"],["0606","0705"],["0604","0704"],
                      ["0605","0705"],
                      ["0705","0706"],["0706","0805"],
                      ["0805","0806"],["0806","0907"],["0806","0906"],
                      ["0807","0908"],["0807","0907"],
                      ["0808","0908"],
                      ["0904","0905"],
                      ["0905","1004"],
                      ["0906","0906"],["0906","1006"],
                      ["0908","0909"],
                      ["0909","1009"],["0909","1008"],
                      ["0910","1010"],["0910","1009"],
                      ["1001","1102"],["1001","1101"],
                      ["1002","1103"],["1001","1102"],
                      ["1003","1103"],
                      ["1004","1005"],
                      ["1005","1006"],["1005","1106"],["1005","1105"],
                      ["1103","1104"],["1104","1203"],
                      ["1105","1106"],["1105","1205"],
                      ["1203","1204"],
                      ["1204","1205"],["1204","1305"],["1204","1304"],
                      ["1304","1305"],["1305","1404"],
                      ["1404","1405"]]
    riverStr       = (':' +
                      ':'.join([shex(h1)+shex(h2)+":"+shex(h2)+shex(h1)
                                for h1,h2 in overRiver]) +
                      ":")
    sovietHexes    = ['0301','0302','0303','0304',
                      '0405',
                      '0504','0505','0506','0507','0508','0509','0510',
                      '0803']
    # --- Combat resolution table ------------------------------------
    crtTable       = [['NE','NE', 'NE', 'NE', 'NE', 'NE'], # 0
                      ['AL','NE', 'NE', 'EX', 'EX', 'DR'], # 1
                      ['NE','NE', 'EX', 'EX', 'DR', 'DR'], # 2
                      ['EX','EX', 'DR', 'DR', 'DR', 'DRL'],# 3
                      ['EX','DR', 'DR', 'DRL','DRL','DE'], # 4
                      ['DR','DRL','DRL','DRL','DE', 'DE'], # 5
                      ['DR','DRL','DE', 'DE', 'DE', 'DE']  # 6
                      ]
    crt            = [':'.join(f'(Die=={n+1})?"{r}"'
                               for n,r in enumerate(row))+':"NE"'
                      for row in crtTable]
    results        = ['NE','AL','EX','DR','DRL','DE']
    # --- Weather ----------------------------------------------------
    weathers       = [['Clear',key(NONE,0)+',setClear',[1,2,5,6,7,14]],
                      ['Mud',  key(NONE,0)+',setMud',  [3,4,13]],
                      ['Snow', key(NONE,0)+',setSnow', [8,9,10,11,12]]]

    #-----------------------------------------------------------------
    # Get the game and do some initial settings 
    game                       = build.getGame()
    maps                       = game.getMaps();
    restore                    = maps['DeadMap'].getMassKeys()['Restore']
    main                       = maps['Board']
    mkeys                      = main.getMassKeys()
    mkeys['Eliminate']['icon'] = getImage('eliminate-icon.png')
    mkeys['Flip']     ['icon'] = getImage('flip-icon.png')
    restore           ['icon'] = getImage('restore-icon.png')
    game.remove(maps['DeadMap'])
    # main.append(restore)
    main['tickness']           = 1
    main['color']              = rgb(132,0,0)
    main['moveKey']            = reinforceMove
    stacking                   = main.getStackMetrics()[0]
    stacking['exSepX']         = 10
    stacking['exSepY']         = 18
    stacking['unexSepX']       = 6
    stacking['unexSepY']       = 10
    stacking['disabled']       = False

    details = main.getCounterDetailViewer()[0]
    details['bgColor'] = rgb(0xCc,0xCC,0xCC)
    details['unrotatePieces'] = True
    
    # ----------------------------------------------------------------
    # Get the prototypes container 
    prototypesContainer = game.getPrototypes()[0]
    prototypes          = prototypesContainer.getPrototypes()

    # ----------------------------------------------------------------
    # Set up global preferences 
    go = game.getGlobalOptions()[0]
    bp = go.getBoolPreferences()
    bp['wgAutoOdds']   ['default'] = True
    bp['wgAutoResults']['default'] = True
    
    # ----------------------------------------------------------------
    # Set up global properties 
    gp   = game.getGlobalProperties()[0];
    gp.addProperty(name         = globalFortress,
                   initialValue = fortressStr,
                   description  = 'List of fortification hexes')
    gp.addProperty(name         = globalForest,
                   initialValue = forestStr,
                   description  = 'List of forest hexes')
    gp.addProperty(name         = globalCross,
                   initialValue = riverStr,
                   description  = 'List of river edges')
    gp.addProperty(name         = globalHex,
                   initialValue = '',
                   description  = 'Hex of defender')
    gp.addProperty(name         = globalEdge,
                   isNumeric    = True,
                   initialValue = -1,
                   description  = 'Attackers behind river')
    gp.addProperty(name         = globalCombat,
                   isNumeric    = True,
                   initialValue = True,
                   description  = 'Not in combat phase')
    gp.addProperty(name         = globalWeather,
                   initialValue = weathers[0][0],
                   isNumeric    = False,
                   description  = 'Current combat number')
    gp.addProperty(name         = germanRP,
                   initialValue = 0,
                   isNumeric    = True)
    gp.addProperty(name         = sovietRP,
                   initialValue = 0,
                   isNumeric    = True)
    # ----------------------------------------------------------------
    # Set up some global keys on the main map
    # {(GetProperty("{wgBattleNo")==wgCurrentBattle) && (wgBattleUnit==true)}
    curBtl        = (f'{{{battleNo}=={currentBattle}&&'
                     f'{battleUnit}==true}}')
    curDef        = (f'{{{battleNo}=={currentBattle}&&'
                     f'{battleUnit}==true&&'
                     f'IsAttacker==false}}')
    curAtt        = (f'{{{battleNo}=={currentBattle}&&'
                     f'{battleUnit}==true&&'
                     f'IsAttacker==true}}')
    keys                     = main.getMassKeys()
    userMark                 = keys['User mark battle']
    selMark                  = keys['Selected mark battle']
    userMark['canDisable']   = True
    userMark['propertyGate'] = globalCombat
    selMark ['canDisable']   = True
    selMark ['propertyGate'] = globalCombat
    
    
    main.addMassKey(name         = 'Reinforce a unit',
                    buttonHotkey = reinforceKey,
                    buttonText   = '',
                    icon         = getImage('reinforce-icon.png'),
                    hotkey       = reinforceKey,
                    singleMap    = False),
    main.addMassKey(name         = 'Check for combat phase',
                    buttonHotkey = checkCombat,
                    buttonText   = '',
                    hotkey       = checkCombat,
                    singleMap    = False,
                    target       = '',
                    filter       = f'{{BasicName=="{hidden}"}}')
    main.addMassKey(name         = 'Check for german RPs',
                    buttonHotkey = checkGermanRP,
                    buttonText   = '',
                    hotkey       = checkGermanRP,
                    singleMap    = False,
                    target       = '',
                    filter       = f'{{BasicName=="{hidden}"}}')
    main.addMassKey(name         = 'Check for combat phase',
                    buttonHotkey = checkSovietRP,
                    buttonText   = '',
                    hotkey       = checkSovietRP,
                    singleMap    = False,
                    target       = '',
                    filter       = f'{{BasicName=="{hidden}"}}')
    main.addMassKey(name         = 'Store defender hex',
                    buttonHotkey = updateHex,
                    buttonText   = '',
                    singleMap    = False,
                    hotkey       = updateHex,
                    target       = '',
                    filter       = curDef,
                    reportFormat = (f'{{{debug}?("Mass update hex -"'
                                    f'+{globalHex}+"-"):""}}'))
    main.addMassKey(name         = 'Check for attack over river',
                    buttonHotkey = updateEdge,
                    buttonText   = '',
                    hotkey       = updateEdge,
                    singleMap    = False,
                    target       = '',
                    filter       = curAtt,
                    reportFormat = (f'{{{debug}?("Mass update edge "'
                                    f'+{globalEdge}):""}}'))
    main.addMassKey(name         = 'Resolve',
                    buttonText   = '',
                    icon         = '',
                    buttonHotkey = resolveKey,
                    hotkey       = resolveKey,
                    canDisable   = True,
                    propertyGate = globalCombat)
    
    # ----------------------------------------------------------------
    game.getChartWindows()['OOBs']['icon']     = getImage('oob-icon.png')
    game.getSymbolicDices()['1d6Dice']['icon'] = getImage('dice-icon.png')
    game.getPieceWindows()['Counters']['icon'] = getImage('unit-icon.png')

    # ----------------------------------------------------------------
    # Get the OOB and adjust the grid on it
    oob         = maps['OOB']
    oob['icon'] = getImage('oob-icon.png')
    zone        = oob.getBoards()['OOB'].getZones()['OOB']
    sgrid       = zone.getSquareGrids()[0]
    snum        = sgrid.getNumbering()[0]
    dx, dy      = sgrid['dx'], sgrid['dy']
    x0, y0      = sgrid['y0'], sgrid['y0']
    # SVG: 51.25061205934415 38.438102266782984 19 19
    # PNG: 105.48303629514619 75.7086557175181 37 37
    # print(f'{dx},{dy},{x0},{y0}')
    fac         = 0.45 if oob['icon'].endswith('.svg') else 1
    xoff        = 20   if oob['icon'].endswith('.svg') else 0
    sgrid.setAttributes(dx      = int(fac * 140),
                        dy      = int(fac * 70),
                        x0      = int(fac * (75-xoff)),
                        y0      = int(fac * 23))
    snum.setAttributes(hLeading = 0,
                       vLeading = 0,
                       hOff     = 1,
                       vOff     = 0,
                       vDescend = False,
                       sep      = 'O')

    # ----------------------------------------------------------------
    # Get the main turn track zone on main board and adjust grid 
    zone    = main.getBoards()['Board'].getZones()['hex']
    hgrid   = zone.getHexGrids()[0]
    hnum    = hgrid.getNumbering()[0]
    hnum.setAttributes(sep      = hexsep)
    
    # ----------------------------------------------------------------
    # Get the main turn track zone on main board and adjust grid 
    zone    = main.getBoards()['Board'].getZones()['turn track']
    sgrid   = zone.getSquareGrids()[0]
    snum    = sgrid.getNumbering()[0]
    sgrid.setAttributes(color = rgb(255,255,0),
                        x0    = int(fac * (54-xoff)),
                        y0    = int(fac * 64),
                        dx    = int(fac * 119),
                        dy    = int(fac * 119))
    snum.setAttributes(hOff     = -11,
                       vOff     = 1,
                       vDescend = False,
                       sep      = 'T')

    # ----------------------------------------------------------------
    # Get the turns interface, and set up.  We built a set of traits
    # to add to the game turn marker to progress the turn track.
    turns                 = game.getTurnTracks()['Turn']
    turns['reportFormat'] = '{"<b>Phase</b>:  <u>"+Phase+"</u>"}'
    phases                = turns.getLists()['Phase']
    phases['list']        = ','.join(phaseNames)
    gtraits               = []
    turns.addHotkey(hotkey = nextPhase,
                    match  = f'{{Turn==1&&Phase=="{germanSpecial}"}}'),
    for t in range(1,15):
        k     = key(NONE,0)+f',Turn{t}'
        t1    = (t-1) // 7
        t2    = int((t-1) % 7) + 1
        lc    = f'{t1}T{t2}'
        turns.addHotkey(hotkey = k,
                        match  = f'{{Turn=={t}&&Phase=="{germanRepl}"}}',
                        reportFormat = f'<b>=== Turn {t} ===</b>',
                        name         = f'Turn {t}')
        main.addMassKey(name         = f'Turn {t}',
                        buttonHotkey = k,
                        hotkey       = k,
                        target       = '',
                        filter       = '{BasicName=="turn"}')
        gtraits.append(SendtoTrait(mapName     = 'Board',
                                   boardName   = 'Board',
                                   name        = '',#f'To turn {t} ({lc})',
                                   key         = k,
                                   restoreName = '',
                                   zone        = 'turn track',
                                   destination = 'G',
                                   position    = lc))
    # --- Add global commands for the interface to turn track --------
    main.addMassKey(name         = 'Flip turn counter',
                    buttonHotkey = nextFaction,
                    hotkey       = key('F'),
                    target       = '',
                    filter       =  '{BasicName=="turn"}')
    # --- Add commands to turn track to do stuff, and provide reminders
    turns.addHotkey(hotkey = checkCombat,
                    name   = 'Check for (any) combat phase')
    keys = turns.getHotkeys(asdict=True)
    keys['Clear battle markers']['match'] = (
        f'{{Phase=="{germanCombat}"'
        f'||Phase=="{germanMove}"'
        f'||Phase=="{sovietCombat}"'
        f'||Phase=="{sovietMove}"'
        f'}}')

    turns.addHotkey(hotkey = specialAvail,
                    match  = f'{{Turn==4&&Phase=="{sovietRepl}"}}',
                    name   = 'Soviet special unit available',
                    reportFormat = '~ Soviet 1st Shock army available')
    turns.addHotkey(hotkey = tankAvail,
                    match  = f'{{Turn==13&&Phase=="{sovietRepl}"}}',
                    name   = 'Soviet tank unit available',
                    reportFormat = '~ Soviet 1st Armoured army available')
    turns.addHotkey(name = 'German replacement points',
                    hotkey = checkGermanRP,
                    match  = f'{{Phase=="{germanRepl}"}}'#,
                    #reportFormat = '~ 1 German step replacement available'
                    )
    turns.addHotkey(name = 'Number of Soviet replacement points',
                    hotkey = checkSovietRP,
                    match  = f'{{Phase=="{sovietRepl}"}}'#,
                    #reportFormat = '~ 5 Soviet step replacements available'
                    )
    turns.addHotkey(name = 'Flip turn',
                    hotkey = nextFaction,
                    match  = f'{{Phase=="{germanRepl}"||Phase=="{sovietRepl}"}}')
    # --- Mud and snow turns match expressions ------------------------
    # mudExpr  = '||'.join(f'Turn=={t}' for t in weathers[1][2])
    # snowExpr = '||'.join(f'Turn=={t}' for t in weathers[2][2])
    mudExpr  = f'{globalWeather}=="{weathers[1][0]}"'
    snowExpr = f'{globalWeather}=="{weathers[2][0]}"'
    # --- Set the wheather --------------------------------------------
    for name,cmd,lturns in weathers:
        expr = '||'.join(f'Turn=={t}' for t in lturns)
        turns.addHotkey(name   = name,
                        hotkey = cmd+'Top',
                        match  = (f'{{({expr})&&'
                                  f'(Phase=="{germanRepl}")'
                                  f'}}'),
                        )
        main.addMassKey(name         = name,
                        buttonText   = '',
                        icon         = '',
                        buttonHotkey = cmd+'Top',
                        hotkey       = cmd,
                        target       = '',
                        filter       = f'{{BasicName=="turn"}}',
                        )
    
    # --- Stuff on mud and snow --------------------------------------
    turns.addHotkey(name = 'Mud, 1 hex',
                    hotkey       = '',
                    match       = (f'{{({mudExpr})&&'
                                   f'(Phase.contains("movement")&&'
                                   f'!Phase.contains("rail"))}}'),
                    reportFormat = '~ Mud turn, 1 hex max')
    turns.addHotkey(name = 'Mud, half CF',
                    hotkey = '',
                    match  = f'{{({mudExpr})&&Phase.contains("combat")}}',
                    reportFormat = '~ Mud turn, Half (rounded down) CF')
    turns.addHotkey(name = 'Snow turn, MF',
                    hotkey = '',
                    match  = f'{{({snowExpr})&&Phase.contains("movement")}}',
                    reportFormat = '~ Snow turn, Infantry -1MF, Armoured -2MF')
    # --- End of game alerts -----------------------------------------
    turns.addHotkey(name         = 'End of extended game',
                    hotkey       = '',
                    match        = '{Turn>=15}',
                    reportFormat = '{Alert("End of game")}')
    turns.addHotkey(name         = 'End of regular game',
                    hotkey       = '',
                    match        = f'{{Turn==8&&Phase=="{germanRepl}"}}',
                    reportFormat = '{Alert("End of regular game")}')
    # --- React to turn commands -------------------------------------
    main.addMassKey(name         = 'Make Soviet special unit avilable',
                    buttonHotkey = specialAvail,
                    buttonText   = '',
                    hotkey       = elimKey,
                    target       = '',
                    singleMap    = False,
                    filter       = '{BasicName=="soviet 1S"}',
                    reportFormat = '~')
    main.addMassKey(name         = 'Make Soviet tank unit avilable',
                    buttonHotkey = tankAvail,
                    buttonText   = '',
                    hotkey       = elimKey,
                    target       = '',
                    singleMap    = False,
                    filter       = '{BasicName=="soviet TU"}',
                    reportFormat = '~')
    # ----------------------------------------------------------------
    # Battle unit prototype update with forest, fortress, overriver rules
    battleUnitP = prototypes[battleUnit]
    traits      = battleUnitP.getTraits()
    basic       = traits.pop()
    effAF       = Trait.findTrait(traits,CalculatedTrait.ID,
                                  key='name',value='EffectiveAF')
    effDF       = Trait.findTrait(traits,CalculatedTrait.ID,
                                  key='name',value='EffectiveDF')
    oddsS       = Trait.findTrait(traits,CalculatedTrait.ID,
                                  key='name',value='OddsShift')
    if effAF: traits.remove(effAF)
    if effDF: traits.remove(effDF)
    if oddsS: traits.remove(oddsS)

    traits.extend([
        CalculatedTrait(
            name = 'EffectiveAF',
            expression = (fr'{{CF\/((IsAttacker&&'
                          f'({globalWeather}=="{weathers[1][0]}"))?2:1)}}'),
            description = 'Calculate effectice CF'),
        CalculatedTrait(
            name = 'EffectiveDF',
            expression = '{DF}',
            description = 'Calculate effectice CF'),
        CalculatedTrait(
            name = 'InForest',
            expression = f'{{{globalForest}.contains(":"+LocationName+":")}}',
            description = 'Check if unit is in forest hex'),
        CalculatedTrait(
            name = 'InMoscow',
            expression = '{LocationName=="1103"}',
            description = 'Check if unit is in forest hex'),
        CalculatedTrait(
            name = 'InFortress',
            expression = f'{{{globalFortress}.contains(":"+LocationName+":")}}',
            description = 'Check if unit is in fortress'),
        CalculatedTrait(
            name = 'OverEdge',
            expression = (f'{{{globalCross}.contains(":"+'
                          f'{globalHex}+LocationName+":")}}'),
            description = 'Check if unit attacks across river'),
        CalculatedTrait(
            name = 'OddsShift',
            expression = (f'{{IsAttacker?{battleShift}:'
                          f'((InForest||InMoscow||(Faction=="Soviet"&&'
                          f'InFortress))?-1:0)}}')),
        GlobalPropertyTrait(
            ['',updateHex,GlobalPropertyTrait.DIRECT,
             f'{{(IsAttacker?{globalHex}:LocationName)}}'],
            name        = globalHex,
            numeric     = True,
            description = 'Update defender hex to this unit',
        ),
        GlobalPropertyTrait(
            ['',updateEdge,GlobalPropertyTrait.DIRECT,
             f'{{(IsAttacker?((OverEdge?-1:0)>{globalEdge}?0:{globalEdge}):'
             f'{globalEdge})}}'],
            name        = globalEdge,
            numeric     = True,
            description = 'Update whether over river edge',
        ),
        ReportTrait(updateEdge,
                    report = (f'{{{debug}?("! "+BasicName+" Update edge "+'
                              f'OverEdge+" -> "+{globalEdge}+'
                              f'" forest="+InForest+'
                              f'" fortress="+InFortress+'
                              f'" Moscow="+InMoscow+'
                              f'" OverEdge="+OverEdge+'
                              f'" Weather="+{globalWeather}'
                              f'):""}}')),
        ReportTrait(updateHex,
                    report = (f'{{{debug}?("! "+BasicName+" in "+'
                              f'LocationName+" update hex -> "+'
                              f'{globalHex}+'
                              f'" attacker="+IsAttacker+'
                              f'" forest="+InForest+'
                              f'" fortress="+InFortress+'
                              f'" Moscow="+InMoscow+'
                              f'" OddsShift="+OddsShift+'
                              f'" Weather="+{globalWeather}'
                              f'):""}}')),
        basic
    ])
    rest = RestrictCommandsTrait(keys          = [key('X')],
                                 name          = 'Disable when not in combat',
                                 hideOrDisable = RestrictCommandsTrait.DISABLE,
                                 expression    = f'{{{globalCombat}==true}}')
    battleUnitP.setTraits(rest,*traits)

    # ----------------------------------------------------------------
    # Battle calculation update with forest, fortress, overriver rules
    battleCalcP = prototypes[battleCalc]
    traits      = battleCalcP.getTraits()
    basic       = traits.pop()
    calcShift   = Trait.findTrait(traits,TriggerTrait.ID,
                                  key='key',         
                                  value=key(NONE,0)+',wgCalcBattleShift')
    shiftProp   = Trait.findTrait(traits,CalculatedTrait.ID,
                                  key='name',value='OddsShift')
    shiftProp['expression'] = f'{{wgBattleShift+{globalEdge}}}'

    
    traits.extend([
        # --- Set not combat phase ----------------------------------
        GlobalPropertyTrait(
            ['',checkGermanRP,GlobalPropertyTrait.DIRECT,
             f'{{Phase=="{germanRepl}"?1:0}}'],
            name = germanRP,
            numeric = True,
            description = 'Set german replacements'),
        GlobalPropertyTrait(
            ['',checkSovietRP,GlobalPropertyTrait.DIRECT,
             f'{{Phase=="{sovietRepl}"?5:0}}'],
            name = sovietRP,
            numeric = True,
            description = 'Set soviet replacements'),
        ReportTrait(
            checkGermanRP,
            report = (f'{{"German faction has "+'
                      f'{germanRP}+" replacements"}}')),
        ReportTrait(
            checkSovietRP,
            report = (f'{{"Soviet faction has "+'
                      f'{sovietRP}+" replacements"}}')),
        GlobalPropertyTrait(
            ['',checkCombat,GlobalPropertyTrait.DIRECT,
             f'{{Phase!="{germanCombat}"&&Phase!="{sovietCombat}"}}'],
            name = globalCombat,
            numeric = True,
            description = 'Check for (any) combat phase'),
        GlobalPropertyTrait(
            ['',resetEdge,GlobalPropertyTrait.DIRECT,'{-1}'],#Assume river
            name        = globalEdge,
            numeric     = True,
            description = 'Reset river odds shift',
        ),
        GlobalHotkeyTrait(
            name         = '',
            key          = updateHex,
            globalHotkey = updateHex,
            description  = 'Ask GKC to update defender hex'),
        GlobalHotkeyTrait(
            name         = '',
            key          = updateEdge,
            globalHotkey = updateEdge,
            description  = 'Ask GKC to update shift'),
        ReportTrait(updateHex,
                    report = (f'{{{debug}?("! "+BasicName+" Update hex "+'
                              f'{globalHex}):""}}')),
        ReportTrait(updateEdge,
                    report = (f'{{{debug}?("! "+BasicName+" Update edge "+'
                              f'{globalEdge}):""}}')),
        basic
        ])
    keys = calcShift.getActionKeys()
    # print(keys,calcShift['actionKeys'])
    calcShift.setActionKeys([resetEdge,updateHex,updateEdge]+keys)
    battleCalcP.setTraits(*traits)

    # ----------------------------------------------------------------
    # Loop over prototypes and fix them up
    oddsP = prototypes['OddsMarkers prototype']
    traits = oddsP.getTraits()
    batres = Trait.findTrait(traits,CalculatedTrait.ID,
                             key='name',value='BattleResult')
    die    = Trait.findTrait(traits,CalculatedTrait.ID,
                             key='name',value='Die')
    die['expression'] = '{{GetProperty("1d6Dice_result")}}'
    traits.remove(batres)
    oddsP.setTraits(*traits)
    
    # ----------------------------------------------------------------
    # Loop over prototypes and fix them up
    for faction in ['Soviet','German']:
        p       = prototypes.get(f'{faction} prototype',None)
        # --- Remove some unwanted traits ----------------------------
        traits  = p.getTraits()
        basic   = traits.pop()
        delt    = Trait.findTrait(traits,DeleteTrait.ID)
        if delt is not None:
            traits.remove(delt)
        rept    = Trait.findTrait(traits,ReportTrait.ID)
        if rept is not None:
            traits.remove(rept)
            
        if verbose:
            print(f'=== {name} ===')

        # --- If we have an eliminate trait, modify it ---------------
        elim = Trait.findTrait(traits,SendtoTrait.ID,
                               'name', 'Eliminate')
        if elim is not None:
            pool = elim['boardName']
            elim['boardName']   = 'Board'
            elim['mapName']     = 'Board'
            elim['destination'] = 'Z'
            elim['zone']        = pool
            elim['restoreKey']  = ''
            elim['restoreName'] = ''
            elim['key']         = elimKey
            elim['name']        = ''

        traits.extend([
            TriggerTrait(
                name       = 'Eliminate',
                command    = 'Eliminate',
                key        = key('E'),
                actionKeys = [stepKey,elimKey],
                property   = '{Phase.contains("combat")}'),
            TriggerTrait(
                name       = 'Step loss',
                command    = 'Step loss',
                key        = key('F'),
                actionKeys = [stepKey+'Real'],
                property   = '{Phase.contains("combat")||OwnReplacement}'
            ),
            TriggerTrait(
                name       = 'Step loss',
                command    = '',
                key        = stepKey+'Real',
                actionKeys = [stepKey,reinforceIncr],
                property   = '{Step_Level==1}'
            ),
            TriggerTrait(
                name       = 'Step loss',
                command    = '',
                key        = stepKey+'Real',
                actionKeys = [elimKey],
                property   = '{Step_Level==2}'
            )])
        
        traits.append(basic)
        p.setTraits(*traits)

    # ----------------------------------------------------------------
    # Add starting units
    #
    # Get all the counter
    counters = game.getPieces(asdict=True)
    sovietHexes    = {'soviet 29':  '0301',
                      'soviet 30':  '0302',
                      'soviet 19':  '0303',
                      'soviet 16':  '0304',
                      # 
                      'soviet 32':  '0803', # Reserve
                      'soviet 20':  '0405',
                      'soviet 24':  '0504',
                      'soviet 43':  '0505',
                      'soviet 33':  '0506',
                      # 
                      'soviet 50':  '0507',
                      'soviet 3':   '0508',
                      'soviet 13':  '0509',
                      'soviet 40':  '0510',
                      }
    germanHexes = { 'german VIII':		'0204',
                    'german XXII':		'0104',
                    'german XXVII':		'0201',
                    'german XLI':		'0202',
                    'german LVI':		'0203',
                    'german V':		        '0103',
                    'german VI':		'0205',
                    #
                    'german VII':		'0106',
                    'german IX':		'0206',
                    'german XX':		'0107',
                    'german XII':		'0307',
                    'german XL':		'0305',
                    'german XLVI':		'0306',
                    'german LVII':		'0406',
                    # 
                    'german XIII':		'0310',
                    'german XLII':		'0209',
                    'german LIII':		'0309',
                    'german XXIV':		'0407',
                    'german XLVII':		'0408',
                    'german XLVIII':		'0409',
                    'german XXXIV':		'0210',
                    'german XXXV':		'0410',
                   }
                    
                    
    # Now loop over all counters, and for the soviet counters,
    # flip state from 1 to 2, since they all start reduced.
    #
    # We also create a mapping from map,loc,board to each counter
    placements = {}
    
    for cn, c in counters.items():        
        # Create a copy of the node and adjust gpid
        destMap   = main
        destLoc   = None
        destBoard = 'Board' 
        traits    = c.getTraits()
        basic     = traits.pop()
        side      = False
        if 'soviet' in cn or 'german' in cn:
            layer  = Trait.findTrait(traits,LayerTrait.ID,
                                     key='name',value='Step')
            faction = cn.split(' ')[0].capitalize()
            repl    = germanRepl if 'german' in cn else sovietRepl
            rp      = germanRP if 'german' in cn else sovietRP
            # elim   = Trait.findTrait(traits,SendtoTrait.ID,
            #                          key='name',value=key('E'))

            layer['loop']         = False
            layer['activateKey']  = ''
            layer['activateName'] = ''
            layer['increaseName'] = ''
            layer['increaseKey']  = stepKey
            layer['decreaseName'] = ''
            layer['decreaseKey']  = ''
            layer['resetName']    = ''
            layer['resetKey']     = reinforceReal
            # elim ['key']          = elimKey

            print(f'{rp} {repl}')
            traits.extend([
                CalculatedTrait(
                    name='SpecialReplace',
                    expression=(f'{{Turn==1&&Phase=="{repl}"}}' if rp==germanRP
                                else f'{{1!=1}}')),
                CalculatedTrait(
                    name = 'OwnReplacement',
                    expression = f'{{Phase=="{repl}"}}'),
                RestrictCommandsTrait(
                    keys=[reinforceKey],
                    expression=(f'{{!OwnReplacement||'
                                f'Step_Level!=2}}')),                
                TriggerTrait(
                    name       = 'Reinforce',
                    command    = 'Reinforce',
                    key        = reinforceKey,
                    actionKeys = [reinforceReal,reinforceDecr],
                    property   = (f'{{OwnReplacement&&'
                                  f'Step_Level==2&&'
                                  f'{rp}>0}}')),
                TriggerTrait(
                    name       = 'Replace',
                    command    = '',
                    key        = reinforceMove,
                    actionKeys = [reinforceMove+'2'],
                    property   = (f'{{!SpecialReplace&&'
                                  f'OwnReplacement&&'
                                  f'CurrentZone=="hex"&&'
                                  f'OldZone.contains("pool")}}')
                ),
                TriggerTrait(
                    name       = 'Replace',
                    command    = '',
                    key        = reinforceMove+'2',
                    actionKeys = [reinforceDecr],
                    property   = (f'{{{rp}>0}}')
                ),
                # Order important - yes it is!
                TriggerTrait(
                    name       = 'Replace',
                    command    = '',
                    key        = reinforceMove+'2',
                    actionKeys = [elimKey],
                    property   = (f'{{{rp}<=0}}')
                ),
                TriggerTrait(
                    name       = 'Replace',
                    command    = '',
                    key        = reinforceMove,
                    actionKeys = [reinforceIncr],
                    property   = (f'{{OwnReplacement&&'
                                  f'CurrentZone.contains("pool")&&'
                                  f'OldZone=="hex"}}')
                ),
                # Undo illigal move
                ReportTrait(
                    reinforceMove+'2',
                    report = (f'{{(!SpecialReplace&&{rp}<=0)?'
                              f'("~{faction} has no more replacement points")'
                              f':""}}')),
                GlobalPropertyTrait(
                    ['',reinforceDecr,GlobalPropertyTrait.INCREMENT,'{-1}'],
                    ['',reinforceIncr,GlobalPropertyTrait.INCREMENT,'{1}'],
                    name = rp,
                    numeric = True,
                    min     = 0,
                    max     = 5),
                # ReportTrait(
                #     reinforceMove,
                #     report = (f'{{"{faction} moved in replacement "+BasicName}}')),                
                ReportTrait(
                    reinforceDecr,reinforceIncr,
                    report = (f'{{(Phase!="{germanRepl}"||Turn!=1)?("{faction} has "+{rp}+" replacement points left"):""}}'))])
                          
                    
                    
                    

            
            # --- Soviet unit, find next hex -----------------------------
            if 'soviet' in cn:
                side = True
                print(f'--- Setting state of {cn} to 2')
                layer['level'] = 2
             
                if '1S' in cn:
                    placements.setdefault(('4O1',oob,'OOB'),[]).append(c)
                elif 'TU' in cn:
                    placements.setdefault(('6O3',oob,'OOB'),[]).append(c)
                else:
                    hex = sovietHexes.get(cn,'Soviet pool')
                    if len(hex) == 4:
                        hex = shex(hex)
                    placements.setdefault((hex,main,'Board'),[]).append(c)
            elif 'german' in cn:
                # --- All german units in pool ---------------------------
                side = True

                hex = germanHexes.get(cn,'German pool')
                if hex == 'German pool':
                    print(f'{cn:30} missing')

                traits.extend([
                    RestrictCommandsTrait(
                        keys=[key('D')],
                        expression=(f'{{Phase!="{germanRepl}"||'
                                    f'Turn!=1}}')),
                    TriggerTrait(
                        name       = 'Deploy semi-realistic',
                        command    = 'Deploy semi-realistic',
                        key        = key('D'),
                        actionKeys = [deployKey],
                        property   = f'{{Turn==1&&Phase=="{germanRepl}"}}'
                    ),
                    SendtoTrait(
                        key         = deployKey,
                        mapName     = main['mapName'],
                        boardName   = 'Board',
                        name        = '',
                        restoreName = '',
                        restoreKey  = '',
                        destination = SendtoTrait.GRID,
                        position    = shex(hex)
                    ),
                ])
                placements.setdefault(('German pool',main,'Board'),[])\
                          .append(c)
            
        elif 'turn' in cn:
            # --- Turn maker at turn track, also add prototype -------
            placements.setdefault(('0T1',main,'Board'),[]).append(c)
            report = Trait.findTrait(traits,ReportTrait.ID);
            if report is not None:
                traits.remove(report)

            weatherCmds = [['',cmd,GlobalPropertyTrait.DIRECT,
                            f'{{"{name}"}}']
                           for name,cmd,_ in weathers]
            traits.append(GlobalPropertyTrait(
                *weatherCmds,
                name        = globalWeather,
                numeric     = False,
                description = 'Weather'))
            traits.extend(gtraits)
            traits.append(RestrictAccessTrait(
                sides=[],
                description='Cannot move'))
        elif 'odds marker' in cn:
            # n = self._oddsMarks.index(cn)+1
            n = int(cn.replace('odds marker','').replace(':1',''))
            # print(f'Odds marker {cn} -> {n} -> {crt[n]}')
            traits.extend([
                CalculatedTrait(
                    name        = 'BattleResult',
                    expression  = crt[n],
                    description = 'Resolve combat'),
            ])
        if side:
            # If this was a side unit, then extract the CF and MF 
            fac = Trait.findTrait(traits,MarkTrait.ID,
                                  'name','factors')
            if fac is not None:
                raw     = fac['value']
                text    = raw.replace('chit 2 factors=','').strip()
                factors = [int(s) for s in text.split(' ')]
                cf      = factors[0]
                mf      = factors[1]
                cft     = MarkTrait(name='RawCF',value=cf)
                mft     = MarkTrait(name='RawMF',value=mf)
                traits.extend([cft,mft])

        # --- Set the traits -----------------------------------------
        traits.append(basic)
        c.setTraits(*traits)
            

    # ----------------------------------------------------------------
    # Having stored the placements, we can loop over these and add
    # stacks.
    for place, c in placements.items():
        destLoc, destMap, destBoard = place
        a = destMap.addAtStart(name            = destLoc,
                               location        = destLoc,
                               owningBoard     = destBoard,
                               useGridLocation = True)
        a.addPieces(*c)

    # ----------------------------------------------------------------
    # Remove some pieces from the piece window, as we've placed them
    # on the map, and there's no need for the players to pick up new
    # ones.
    pwindows = game.getPieceWindows()
    for pw in pwindows.values():
        print(f'Piece window: {pw["name"]}')
        tab = pw.getTabs().get("Counters",None)
        if not tab:
            continue

        panels = tab.getPanels()
        german = panels.get("German",None)
        soviet = panels.get("Soviet",None)
        if german:
            tab.remove(german)
        if soviet:
            tab.remove(soviet)
        
    # ----------------------------------------------------------------
    # Some more documentation 
    doc = game.getDocumentation()[0]
    vmod.addFile('help/more.html',moreInfo.format(title=game['name']))
    doc.addHelpFile(title='More informtion', fileName='help/more.html')
#
# EOF
#
