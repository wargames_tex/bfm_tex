![Logo](.imgs/logo.png "Logo")

# Battle for Moscow 

[[_TOC_]]

My version of the board wargame.

Original page: [grognard,com][]. 

This uses my [wargame][] $`\LaTeX`$ package.

## Content 


- [BattleForMoscowA4.pdf][] Everything in one file: Rules, counters,
  boards, and charts.
- [BattleForMoscowA4Booklet.pdf][] The rules formatted into a booklet.
  Print on a duplex A4 printer with long-edge binding.  Fold the
  sheets into double A5 sheets and stable through the fold to create a
  nice booklet.
- [BattleForMoscowA3Booklet.pdf][] The rules formatted into a booklet.
  Print on a duplex A3 printer with long-edge binding.  Fold the 
  sheets into double A4 sheets and stable through the told to create 
  a nice booklet.
- [materialsA4.pdf][] The game boards (basic and extended), charts
  (basic and extended), and counters. 


The documents are designed to be printed on an A4 printer.  If you do
not have access to an A4 printer (say because you're in the US which
still hasn't figured out that almost the rest of the world is using a
more sensible format than Letter), then your can use these equivalent
files

| ISO                              | Letter                                |
|---------------------------------:|:--------------------------------------|
| [BattleForMoscowA4.pdf][]        | [BattleForMoscowLetter.pdf][]         |
| [BattleForMoscowA4Booklet.pdf][] | [BattleForMoscowLetterBooklet.pdf][]  |
| [BattleForMoscowA3Booklet.pdf][] | [BattleForMoscowTabloidBooklet.pdf][] |
| [materialsA4.pdf][]              | [materialsLetter.pdf][]               |

Download [artifacts.zip][] for all files, or [browse][] files. 

## VASSAL module 

There is a VASSAL module generated from the above documents via
[`export.tex`](export.tex), the Python script `wgexport.py` from the
[wargame][] package, and the local patching script
[`patch.py`](patch.py).

- [BattleForMoscow.vmod][]

## Previews 

![Basic board](.imgs/board.png "Board")
![Expansion board](.imgs/board2.png "Expansion board")
![Coloured counters](.imgs/counters.png "Counters")
![Charts](.imgs/charts.png "Charts")
![Front page](.imgs/front.png "Front page")
![VASSAL module](.imgs/vassal.png "VASSAL module")
![Photo](.imgs/photo.jpg "Photo")

(Note that the photo is somewhat out of date)

[grognard,com]: http://grognard.com/bfm
[wargame]: https://gitlab.com/wargames_tex/wargame_tex

[browse]: https://gitlab.com/wargames_tex/bfm_tex/-/jobs/artifacts/master/browse?job=dist
[artifacts.zip]: https://gitlab.com/wargames_tex/bfm_tex/-/jobs/artifacts/master/download?job=dist

[BattleForMoscowA4.pdf]: https://gitlab.com/wargames_tex/bfm_tex/-/jobs/artifacts/master/file/BattleForMoscow-A4-master/BattleForMoscowA4.pdf?job=dist
[BattleForMoscowA4Booklet.pdf]: https://gitlab.com/wargames_tex/bfm_tex/-/jobs/artifacts/master/file/BattleForMoscow-A4-master/BattleForMoscowA4Booklet.pdf?job=dist
[BattleForMoscowA3Booklet.pdf]: https://gitlab.com/wargames_tex/bfm_tex/-/jobs/artifacts/master/file/BattleForMoscow-A4-master/BattleForMoscowA3Booklet.pdf?job=dist
[materialsA4.pdf]: https://gitlab.com/wargames_tex/bfm_tex/-/jobs/artifacts/master/file/BattleForMoscow-A4-master/materialsA4.pdf?job=dist

[BattleForMoscowLetter.pdf]: https://gitlab.com/wargames_tex/bfm_tex/-/jobs/artifacts/master/file/BattleForMoscow-Letter-master/BattleForMoscowLetter.pdf?job=dist
[BattleForMoscowLetterBooklet.pdf]: https://gitlab.com/wargames_tex/bfm_tex/-/jobs/artifacts/master/file/BattleForMoscow-Letter-master/BattleForMoscowLetterBooklet.pdf?job=dist
[BattleForMoscowTabloidBooklet.pdf]: https://gitlab.com/wargames_tex/bfm_tex/-/jobs/artifacts/master/file/BattleForMoscow-Letter-master/BattleForMoscowTabloidBooklet.pdf?job=dist
[materialsLetter.pdf]: https://gitlab.com/wargames_tex/bfm_tex/-/jobs/artifacts/master/file/BattleForMoscow-Letter-master/materialsLetter.pdf?job=dist

[BattleForMoscow.vmod]: https://gitlab.com/wargames_tex/bfm_tex/-/jobs/artifacts/master/file/BattleForMoscow-A4-master/BattleForMoscow.vmod?job=dist
