#
#
#
NAME		:= BattleForMoscow
VERSION		:= 1.2
VMOD_VERSION	:= 1.2.2
VMOD_TITLE	:= Battle for Moscow
TUTORIAL	:= Tutorial.vlog

STYFILES	:= bfm.sty
DOCFILES	:= BattleForMoscow.tex	\
		   front.tex		\
		   intro.tex		\
		   hobby.tex		\
		   learn.tex		\
		   rules.tex		\
		   history.tex		\
		   next.tex		
DATAFILES	:= materials.tex	\
		   hexes.tex		\
		   tables.tex		\
		   hexes.pdf		
TARGETS		:= $(NAME).pdf		\
		   materials.pdf		
BOOKLETS	:= $(NAME)Booklet.pdf

TARGETSA4	:= $(TARGETS:%.pdf=%A4.pdf) 			\
		   $(BOOKLETS:%Booklet.pdf=%A4Booklet.pdf)	\
		   $(BOOKLETS:%Booklet.pdf=%A3Booklet.pdf)	
TARGETSLETTER	:= $(TARGETS:%.pdf=%Letter.pdf) 		\
		   $(BOOKLETS:%Booklet.pdf=%LetterBooklet.pdf)	\
		   $(BOOKLETS:%Booklet.pdf=%TabloidBooklet.pdf)	

SIGNATURE	:= 16
PAPER		:= --a4paper

include Variables.mk
include Patterns.mk

all:				a4
a4:				$(TARGETSA4) vmod
letter:				$(TARGETSLETTER)
vmod:				$(NAME).vmod
mine: 				a4 cover.pdf box.pdf 

include Rules.mk

$(NAME).aux:			$(DOCFILES)  $(DATAFILES) $(STYFILES)
materials.pdf:		        $(DATAFILES) $(STYFILES)
export.pdf:			export.tex   $(DATAFILES) $(STYFILES)
front.pdf:			front.tex    hexes.pdf    $(STYFILES)

$(NAME)A4.pdf:			$(NAME)A4.aux
$(NAME)A4.aux:			$(DOCFILES)  $(DATAFILES) $(STYFILES)
materialsA4.pdf:		$(DATAFILES) $(STYFILES)

$(NAME)Letter.pdf:		$(NAME)Letter.aux
$(NAME)Letter.aux:		$(DOCFILES)  $(DATAFILES) $(STYFILES)
$(NAME)LetterBooklet.pdf:	$(NAME)Letter.pdf
materialsLetter.pdf:		$(DATAFILES) $(STYFILES)

$(NAME)Rules.aux:		$(DOCFILES)  $(DATAFILES) $(STYFILES)

include Docker.mk

docker-artifacts::
	@echo "VMOD dist"
	$(MUTE)cp $(NAME).vmod $(NAME)-A4-$(VERSION)/


.imgs/front.png:	frontA4.png
	mv $< $@

.imgs/%.png:	%.png
	@echo "MOVE $< -> $@"
	$(MUTE)mv $< $@

charts.pdf:		misc.tex
	$(LATEX) --jobname $(basename $@) $< 

counters.pdf:		misc.tex
	$(LATEX) --jobname $(basename $@) $< 

board.pdf:		hexes.tex
	$(LATEX) --jobname $(basename $@) $< 


board2.pdf:		hexes.tex
	$(LATEX) --jobname $(basename $@) $< 

update-pngs:	\
	.imgs/board.png		\
	.imgs/board2.png	\
	.imgs/charts.png	\
	.imgs/counters.png	\
	.imgs/front.png		

.PRECIOUS:	$(NAME)Rules.pdf $(NAME).vtmp export.pdf export.json 

#
# EOF
#
