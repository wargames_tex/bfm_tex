\twocolumn[\part{Operation Typhoon}]

\emph{Historical Background for the Battle for Moscow game}

On the 22\textsuperscript{nd} of June, 1941 Germany invaded the Soviet
Union. Confident that the Blitzkrieg was invincible, the German high
command expected to overrun most of the European portion of the
U.S.S.R. in a matter of weeks, crushing the Soviets' ability to resist
and securing German mastery of Europe.

Initially successful on all fronts, the German invasion reached deep
into the Soviet Union throughout the summer of 1941. The Soviets,
however, used their large population and abundant resources to
constantly field new forces, replacing the millions of men
lost. Although often poorly equipped and inexpertly led, the Soviet
soldiers learned to fiercely resist the invaders as they defended
Mother Russia. By autumn, the Germans were approaching many key cities
in the Soviet Union: Lenin\-grad, Moscow, Kharkov, and Rostov, but
German reserves were dwindling and the bad weather of autumn was
rapidly approaching.

\textsl{Operation Typhoon:} In September the German High Command
decided to launch a final, decisive offensive for the year to capture
Moscow and break the Red Army --- Operation Typhoon. Gathering forces
from across the entire front, the Germans launched their offensive in
early October. The Soviets did not expect a German offensive so late
in the season and were taken by surprise. The Soviet front line was
quickly shattered and surrounded, with over 600,000 men taken prisoner
in pockets at Vyazma and Bryansk.

While the German infantry cleared the pockets, the panzers raced
towards Moscow. However, the Germans were slowed both by the desperate
resistance of the remaining Soviet forces in the defence lines built
before Moscow and by the weather. The autumn rains had started by
mid--October, and they quickly turned the dirt roads to mud, greatly
restricting German mobility. Even so, by the end of October the
Germans had captured Mozhaisk, the last major town on the direct road
to Moscow, and were approaching Tula on the southern route to the
Soviet capital. At their closest, the Germans were now only 40 miles
from Moscow.

In November the rains slackened, and the Germans would have a short
period of clear weather before the Russian winter began.

However, the German forces had no reserves left, and their strength
was dropping, while fresh, new Soviet forces were arriving at the
front. The Germans resumed the offensive in mid--November. In front of
Moscow, the Germans were stalled as the Soviets launched repeated
counterattacks. In the south the Germans, unable to take Tula,
bypassed the city. In the north the Germans slowly inched forward to
the gates of Moscow. By the 5\textsuperscript{th} of December, the
Germans were halted along the entire front. Exhausted and demoralised,
the German forces had failed to take their objective. The next day,
amidst the snows and intense cold of the onset of winter, the
reinforced Soviets launched their winter counteroffensive.

\tikzset{
  sep30/.style={DarkRed,
    dash pattern=on 12pt off 4pt on 4pt off 4pt,
    line width=8pt},
  oct10/.style={DarkGreen,
    dash pattern=on 12pt off 4pt on 4pt off 4pt on 4pt off 4pt,
    line width=8pt},
  oct30/.style={DarkMagenta,
    dash pattern=on 4pt off 4pt,
    line width=8pt},
  dec5/.style={DarkCyan,
    dash pattern=on 12pt off 4pt,
    line width=8pt},
  german advance/.style={->,line width=4pt},
  soviet advance/.style={->,line width=4pt,gray}
}
  
  
\begin{figure}[htbp]
  \centering
  \begin{tikzpicture}[scale=.5,rotate=90]
    \begin{scope}[
      shift={(hex cs:c=15,r=6)},
      transform shape,
      scale line widths,
      align=left,
      anchor=west]
      \draw[sep30](0,6)--(2,6) node{\textsf{30\textsuperscript{th} of Sep.}};
      %% 
      \draw[oct10](0,5)--(2,5) node{\textsf{10\textsuperscript{th} of Oct.}};
      %% 
      \draw[oct30](0,4)--(2,4) node{\textsf{30\textsuperscript{th} of Oct.}};
      %% 
      \draw[dec5](0,3)--(2,3) node{5\textsf{\textsuperscript{th} of Dec.}};
      %% 
      \draw[german advance](0,1)--(2,1) node{%
        \textsf{German}\\\textsf{attacks}};
      %% 
      \draw[soviet advance](0,0)--(2,0) node{%
        \textsf{Soviet}\\\textsf{counter--attacks}};
    \end{scope}
    \showhistorytrue
    \hexes
    \showhistoryfalse
    \begin{scope}[decoration={bent,aspect=.3,amplitude=-10},
      ->,scale line widths,line width=2pt]
      \draw[decorate,line width=4pt](0201.SE edge)--(0603);
      \draw[decorate](0408.NE edge)--(0608);
      \draw[decorate,line width=4pt](0405.SW edge)--(0603);
      \draw[decorate,line width=4pt](0510.SW edge)--(1008);
      \draw[decorate](0505.NW edge)--(0705);
      \draw[decorate](0502.SW edge)--(0701);
      \draw[decorate](0704.SW edge)--(0803);
      \draw[decorate](0805.SW edge)--(0905);
      \draw[decorate](0804.SW edge)--(0904);
      \draw[decorate](0801.NW edge)--(1001);
      \draw[decorate](0801.SE edge)--(1001);
      \draw[decorate](1207.SW edge)--(1206);
      \draw[decorate,line width=3pt,gray](1103)--(1003);
    \end{scope}
    \draw[decorate,->,line width=2pt](0703.SW edge)--(0802);
    \draw[decorate,->,line width=2pt](0802.SE edge)--(0903);
    \draw[decorate,->,line width=2pt](1108.SW edge)--(1108);
    \draw[decorate,->,line width=2pt](1207.SW edge)--(1307);
    \draw[decorate,->,line width=3pt,gray](1205.N edge)--(1205);
    \draw[decorate,->,line width=3pt,gray](1105.NE edge)--(1105);

    \begin{scope}[decorate,decoration={bent,aspect=.3,amplitude=10},
      ->,scale line widths,line width=2pt]
      \draw[decorate](0510.NW edge)--(0608);
      \draw[decorate,line width=4pt](0102.NW edge)--(0401);
      \draw[decorate](0506.SW edge)--(0706);
      \draw[decorate](0908.SW edge)--(1007);
      \draw[decorate](0902.NW edge)--(1002);
      \draw[decorate](1207.SW edge)--(1308);
      \draw[decorate,line width=3pt,gray](1102.SE edge)--(1001);
      
      \draw[->,line width=2pt](1003.W) .. controls (1103.E) and (0904.E) .. 
      (0904.NE edge);
      %% Soviet counter
      \draw[decorate,line width=3pt,gray]  (1104.SE edge)--(1003);
      % (hex cs:c=11,r=3,v=south west) .. cycle;
    \end{scope}
  \end{tikzpicture}
  \caption{Historical advance of the Germans}
\end{figure}
%% Local Variables:
%%   TeX-master: "BattleForMoscow"
%% End:
