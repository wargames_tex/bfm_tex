\twocolumn[\part{A Complete Board Wargame}]

Operation Typhoon, the German Army's final lunge to capture Moscow in
1941, was intended to break the Soviet Army and end its resistance to
German conquest. If the operation succeeded, it would mean the
collapse of Soviet morale (or so the Germans believed). If it failed,
it would (and did) leave the exhausted Germans open to a Soviet
counter-offensive that would push them forever beyond reach of Moscow.

\textbf{Battle for Moscow} is a historical wargame of the German
Army's struggle to defeat the Soviet Army and capture Moscow in
1941. It is played on a map of the territory where the battle was
fought, and it uses playing pieces which represent the actual military
units (German corps and Soviet armies) from the battle. The game rules
duplicate the situation as it occurred.

\section{How to Learn the Game}
\label{sec:howtolearn}

If you have never played a wargame before, the ideal way to learn is
to have an experienced player teach you.

If you do not have an experienced player handy, just read the rules
through once, paying particular attention to the examples. Be sure to
follow the sequence of play exactly
(\sectionname~\ref{sec:howtoplay}). Refer back to the rules whenever
you have any questions.

\textbf{Experienced Players}: When teaching the game to a novice, you
should play the Soviets; your opponent will have more fun attacking
than defending. Reduce the Soviet replacements from five per turn to
three. You might consider coaching your opponent a bit if he's about
to make a mistake that will cost him the game, but otherwise let him
play his own game.

\section{Game Components}


\subsection{The Counters} 

\begin{figure}
  \centering
  %% \includegraphics{03-01}
  \begin{tikzpicture}[transform shape,
    r/.style args={#1,#2}{draw,shape=ellipse,minimum width=#1cm,
      minimum height=#2cm,outer sep=.05mm},
    l/.style={text width=2cm,inner sep=.05mm,outer sep=0mm},
    n/.style={anchor=north,align=center},
    e/.style={anchor=east,align=right},
    w/.style={anchor=west,align=left},
    s/.style={anchor=south,align=center},
    ne/.style={anchor=north east,align=right},
    nw/.style={anchor=north west,align=left},
    se/.style={anchor=south east,align=right},
    sw/.style={anchor=south west,align=left},
    ]
    % \draw(-2.6,-5) rectangle (6.3,2.5);
    \chit[soviet 5]        (-1,0)
    \chit[soviet 5 flipped](1,0)

    \begin{scope}[mosgreen!50!black,very thick,align=center]
      \draw( 1,   0.45)node[r={.5,.3}]{}--++(30:1)   node[l,sw]{Size};
      \draw( 1.45,0.2) node[r={.3,.4}]{}--++(10:.5)  node[l,w] {Identifier};
      \draw(-1.25,-.4) node[r={.5,.5}]{}--++(-135:.5)node[l,ne]{Combat factor};
      \draw( 1.25,-.4) node[r={.5,.5}]{}--++(-45:.5) node[l,nw]{Movement factor};
      \draw[<-] (-1.4,.2)--++(180:.5) node[l,e]{Unit type};
      \draw[] (-1,1.2) node[l,s]{Full strength};
      \draw[] ( 1,1.2) node[l,s]{Half strength};
    \end{scope}
    
    \begin{scope}[shift={(0,-2.5)}]
      \node[scale=.5,natoapp6c={command=land,faction=friendly,main=infantry}]
      (inf) at (-2.5,0) {};
      \node[scale=.5,natoapp6c={command=land,faction=friendly,main=armoured}]
      (arm) at (-2.5,-1) {};
      \node[scale=.5,natoapp6c={command=land,faction=friendly,echelon=corps}]
      (crp) at (1,0) {};
      \node[scale=.5,natoapp6c={command=land,faction=friendly,echelon=army}]
      (amy) at (1,-1) {};
      \node[right=2mm of inf.east,w] {Infantry};
      \node[right=2mm of arm.east,w] {Armoured};
      \node[right=2mm of crp.east,w] {Corps};
      \node[right=2mm of amy.east,w] {Army};
    \end{scope}
  \end{tikzpicture}
  \caption[Counter parts]{Counter parts}
  \label{fig:combat_counter}
\end{figure}

Counters represent Soviet and German military units. Soviet units are
red/light-red and German units are
greenish-gray. \figref{fig:combat_counter} illustrates the information
on the pair of counters for one unit.


\begin{description}
\item[Unit type] is either infantry (foot soldiers) or pan\-zer
  (armoured); only the Germans have panzers.
\item[Combat factor (\CF)] measures a unit's value in battle; higher
  numbers are stronger.
\item[Movement factor (\MF)] determines how far the unit can move.
\item[Unit size \textmd{\textrm{and}} identification] are purely for
  historical interest and have nothing to do with play.  A corps
  ($\approx50,000$ persons) has 3 `X', and an army ($\approx100,000$
  persons) is designated by 4 `X'.
\end{description}

All units have two counters: a \emph{full-strength} counter and a
\emph{half-strength} counter with about half the combat strength of
the full-strength side.  Losses in combat can reduce a full-strength
unit to a half-strength unit. Replacements can turn a half-strength
unit into a full-strength unit.

\subsection{Map}

The map is divided into hexagons (hexes for short) which define units'
positions just like the squares of a chessboard. Hexes have been
numbered using an XXYY coordinate scheme. The map also shows important
terrain such as forests, cities, fortifications, rivers, and
railroads; the terrain key in \sectionname~\ref{sec:terrain} explains
each terrain type.

\section{How to Play}
\label{sec:howtoplay}

There are seven turns in Battle for Moscow. Each turn
represents one week (Exception: turns 3 and 4 each represent two
weeks, because mud slows the battle). 

\begin{table}[htbp]
  \centering
  \small
  \turnstable
  \caption[Turns]{Turns}
\end{table}

Each turn is divided into eight parts or \emph{phases} (see
\tablename~\ref{tab:turn:sequence}) performed in the exact order given
below. All actions in one phase must be finished before the next phase
can begin. The first four phases are the German player's turn; the
last four are the Soviet player's turn.

\begin{table*}[htbp]
  \centering
  \turnsequence{}
  \caption{Turn sequence}
  \label{tab:turn:sequence}
\end{table*}
\subsection{Zone of Control}
\label{sec:zoc}

\begin{figure}
  \centering
  \begin{tikzpicture}
    \hex(c=1,r=2)
    \hex(c=1,r=3)
    \hex(c=2,r=1)
    \hex(c=2,r=2)
    \hex(c=2,r=3)
    \hex(c=3,r=2)
    \hex(c=3,r=3)
    \chit[german XLVII](hex cs:c=2,r=2)
  \end{tikzpicture}
  \caption{Zone of control}
  \label{fig:zoc}
\end{figure}

Each unit has a \textbf{zone of control} (ZOC) which consists of the
six hexes surrounding it (see \figurename~\ref{fig:zoc}), including
hexes occupied by enemy units. Enemy zones of control have important
effects on movement, combat, and replacement:

\begin{description}
\item[Movement:] A unit entering an enemy ZOC \emph{must} immediately
  end its movement phase.
\item[Combat:] Units \emph{cannot} end their retreat in an enemy zone
  of control (they are eliminated if they do).
\item[Replacements:] Zones of control affect how a path can be
  traced to allow replacements.
\end{description}

\subsection{Movement}
\label{sec:movement}

Units are moved during the movement phases (phas\-es, 4, 6, and 8 of
the turn). Movement works essentially the same way in each phase. Each
unit has a \emph{movement factor} (\MF), which represents the distance
in hexes it can move in one phase. (Exception: a forest hex counts as
two hexes for movement.) In a phase, the player moves any or all of
his units that qualify (only panzers in the panzer movement phase;
only Soviet units on rail lines in the rail movement phase). Units
move one at a time, from hex to hex, in any direction.

\begin{description}
\item[Rail Movement:] In the rail movement phase, any Soviet units
  which \emph{start} the phase on a rail line may move up to four
  hexes. They \emph{must} move only along the rail line, and
  \emph{cannot} pass through enemy ZOC (see
  \sectionname~\ref{sec:zoc}), even if the hex is occupied by a
  friendly unit.  See also \figurename~\ref{fig:rail}. A forest hex
  counts as only one hex for movement in this phase.
\item[Restrictions:] A unit can never enter a hex containing an
  enemy unit. A unit can enter a hex containing a friendly unit, but
  there can only be one unit in a hex at the end of the phase.  That
  is, units may move \emph{through} hexes occupied friendly units.
\item[Zone of Control:] (see \sectionname~\ref{sec:zoc}) A unit which
  enters an enemy ZOC \emph{must} immediately end its movement for the
  phase.  That means that a unit \emph{may} move from one hex in enemy
  ZOC to another hex in enemy ZOC, but then cannot move any further.
\end{description}

See \figref{fig:movement:example} and~\ref{fig:rail} for illustrations.

\begin{figure}
  \centering
  \begin{tikzpicture}
    \draw[clip](hex cs:c=7,r=6,v=W) rectangle(hex cs:c=10,r=10,v=E);
    \hexes
    
    \chit[german XLI](1007)

    \begin{scope}[m/.style={hex/long move=soviet-bg-up!75!black},
      l/.style={hex/move cost}]
      \path[m] (1009)--(0909)node[l]{1}--(0808)node[l]{2}--(0708)node[l]{4};
      \path[m] (1009)--(1008)node[l]{1};
      \path[m] (0909)--(0908)node[l]{2};
      \path[m] (0808)--(0807)node[l]{3}--(0707)node[l]{4};
      \path[m] (0807)--(0806)node[l]{5};
      \path[m] (0807)--(0907)node[l]{4};
      \path[m] (0807)--(0707)node[l]{4};
    \end{scope}

    \chit[soviet 5 flipped](1009)
    
    \begin{scope}[font=\sffamily\bfseries\Large,
      rectangle,
      opacity=.5,
      text opacity=1,
      anchor=north west,
      fill=white]
      \node at(1008.chit NW){C};
      \node at(0908.chit NW){D};
      \node at(0907.chit NW){E};
      \node at(0707.chit NW){A};
      \node at(0708.chit NW){B};
      \node at(0806.chit NW){F};
    \end{scope}
  \end{tikzpicture}
  
  \caption[Movement example]{Movement example. The different ways the
    Soviet 5\textsuperscript{th} Army could move. The numbers show the
    number of hexes it has moved. With a \MF[4], the unit can move
    four hexes, as in path \textsf{A}. In path \textsf{B} the third
    hex the unit enters is a forest hex, which counts as two hexes
    moved, and the unit must stop. In path \textsf{C} the unit enters
    an enemy ZOC in its first hex and must stop. In path \textsf{D}
    the unit enters an enemy ZOC in its second hex and must stop. In
    path \textsf{E}, the unit ends its movement in an enemy ZOC in its
    fourth hex and must stop because its movement allowance is used
    up. Path \textsf{F} is not possible: the unit would have to move 5
    hexes (counting 2 for the forest hex).
    % Paths A, B, and E cost 4 movement
    % factors, while path D and C cost 2 and 1, respectively.  Path F
    % costs 5 movement factors and is not permissible for the Soviet
    % 5\textsuperscript{th} army.
  }
  \label{fig:movement:example}
\end{figure}

\begin{figure}
  \centering
  \begin{tikzpicture}
    \draw[clip](hex cs:c=7,r=7,v=E,o=.5) rectangle(hex cs:c=12,r=2,e=NE);

    \hexes
    
    \chit[german XIII](0804)
    \chit[german XLI](0905)
    \chit[german XL flipped](1205)

    \chit[soviet 33 flipped](1103)
    %\chit[soviet 16 flipped](1105)
    \chit[soviet 49](1106)

    \begin{scope}[font=\Large\bfseries,
      every node/.style={rectangle,
        align=center,
        fill=white,opacity=.7,
        text opacity=1},
      very thick]
      \node at (hex cs:c=12,r=4) {A};
      \node at (hex cs:c=11,r=5) {B};
      \node at (hex cs:c=9,r=4)  {C};
      \node at (hex cs:c=8, r=3) {D};
    \end{scope}
  \end{tikzpicture}
  \caption[Rail movement]{Rail movement. The Soviet
    33\textsuperscript{rd} army can use rail movement up to and
    including the hexes indicated by \textsf{A} through
    \textsf{D}. The Soviet 33\textsuperscript{rd} cannot continue, to
    1107, along the south-bound rail line as it would pass through
    German ZOC, even though it has the \MF{} for it.}
  \label{fig:rail}
\end{figure}

\subsection{Combat}
\label{sec:combat}

In each combat phase (phases 3 and 7), units \emph{may} attack
adjacent enemy units. First, the \emph{attacking faction} (the German
in the German combat phase, the Soviet in the Soviet combat phase)
announces all its battles: which enemy units will be attacked and
which units will attack them. A battle is an attack on one enemy unit
by any or all the attacking faction's units which are adjacent to
it. A single unit may only attack once per phase, and a single enemy
unit may only be attacked once per phase. Once battles have been
announced, the attacking faction cannot change its mind.

\paragraph{Procedure}

Battles are resolved one at a time in any order the attacking faction
decides. For each battle this sequence is followed:

\begin{enumerate}
\item Total the \emph{combat factors} (\CF) of all the attacking
  units, possibly halved (rounded down) due to \emph{mud}.
\item Divide this total by the \CF{} of the defending unit, dropping
  all fractions, to get one of the odds levels given on the combat
  results table. For example, a strength of 16 attacking 4 is 4:1
  (four to one), while 15 attacking 4 is only 3:1.
\item Determine if the effects of terrain have reduced the odds.
\item Roll one die and consult the combat resolution table (CRT);
  cross-index the number rolled with the odds to determine the result.
\item Apply the result immediately.
\item If the attacked unit is no longer in the hex (eliminated or
  forced to retreat), then \emph{one} of the attacking units may
  immediately move into the vacated hex.
\item Go on to the next battle.
\end{enumerate}

\subsection{Maximum and Minimum Odds}

In step 2, if the odds are above 6:1, reduce them to 6:1. After step
3, if the odds are below 1:1 the attack has no effect on either side.

\paragraph{Terrain Effects}
\label{sec:terrain}

If the defending unit is in a forest hex, is in Moscow, or is a
Soviet unit in a fortification, reduce the odds by one level (4:1
becomes 3:1, 3:1 becomes 2:1, and so on). If all the attacking units
are across a river from the defending unit, reduce the odds by one
level. (If both these conditions apply, reduce the odds by two
levels.)

\paragraph{Results}
\label{sec:crt}

\begin{table}[htbp]
  \centering
  \combat{}
  \caption{Combat resolution table}
  \label{tab:combatresults}
\end{table}

There are six different results on the combat resolution table
(\tablename~\ref{tab:combatresults}).

\begin{description}
\item[AL (Attacker Loss):] One attacking unit (of the attacker's
  choice) takes a loss, as described for \textbf{DRL} below (but it
  does not retreat).
\item[NE (No Effect):] The combat as no effect on any units. 
\item[EX (Exchange):] First, the defending unit takes a loss as in
  \textbf{DRL} below. Then, the attacking faction must lose \emph{at
    least} the same amount of strength (\CF) from attacking units. In
  both cases, if a full--strength unit is reduced to half--strength,
  the amount of the loss is the original strength minus the
  reduced strength. For example, if a panzer with a strength of 9
  takes a loss (and is replaced by its strength--4 counter), the loss
  is 5. Finally, the defending unit, if it survives, must retreat as
  for \textbf{DR} below. (Note that the defending unit may be
  eliminated in its retreat, but the attacking player is not required
  to match this loss).
\item[DR (Defender Retreat):]\hspace*{.5em} The defending unit is moved
  two hexes by the \emph{attacking faction}. The unit must end up two
  hexes away from its starting hex and may not enter an enemy zone of
  control. If there is no retreat path which satisfies these
  conditions, the unit is eliminated. The unit also must end its
  retreat in a hex not already occupied by a friendly unit, or must
  retreat further than two hexes if necessary to reach an empty hex.
\item[DRL (Defender Retreat and Loss):] The defending unit must
  first take a loss; then, if it still survives, it must retreat as
  described in \textbf{DR}. If a full--strength unit takes a loss,
  replace it with its half-strength counter. If a half--strength
  unit takes a loss, it is eliminated.
\item[DE (Defender Eliminated):] The defending unit is entirely
  eliminated whether full--strength or half--strength.
\end{description}

\begin{description}
\item[Optional] The factions may choose the optional rule that, if a
  defending unit in a city hex suffers a \textbf{DR} result, it may be
  converted to a single step loss, but not a retreat.
\end{description} 

\begin{figure}
  \centering
  \begin{tikzpicture}
    \begin{scope}
      \draw[clip] (hex cs:c=8,r=5,v=SE) rectangle (hex cs:c=11,r=3,e=SW);%
      \hexes%
      \chit[soviet 5 flipped](1004)%
      \chit[german XLVII](1005)%
      \chit[german VIII](0905)%

      \begin{scope}[a/.style=hex/attack]
        \path[a] (1005)--(1004);
        \path[a] (0905)--(1004);
      \end{scope}
    \end{scope}
  \end{tikzpicture}
  \begin{tikzpicture}
    \begin{scope}
      \draw[clip] (hex cs:c=8,r=5,v=SE) rectangle (hex cs:c=11,r=3,e=SW);%
      \hexes%
      \chit[soviet 5 flipped](1004)%
      \chit[german XLVII](1005)%
      \chit[german VIII](0905)%
      \chit[german XLII flipped](0904)%

      \begin{scope}[a/.style=hex/attack]
        \path[a] (1005)--(1004);
        \path[a] (0905)--(1004);
        \path[a] (0904)--(1004);
      \end{scope}
    \end{scope}
  \end{tikzpicture}
  \caption[Combat example]{Combat example. Two possible attacks on a
    Soviet unit. In the first attack (left) the odds begin at 15 to 4,
    or 3:1 . They are reduced one level for the river and one for the
    defending unit's fortifications, for final odds 1:1.\newline The
    second attack (right) is just like the first except for the
    addition of a strength-2 German infantry unit, but that makes a
    big difference. The odds are now 17 to 4, or 4:1; they are still
    reduced one level for fortifications, but no longer for the river,
    making the final odds 3:1; finally, the Soviet unit would be
    eliminated if forced to retreat, since it is surrounded by enemy
    ZOC.}
  \label{fig:combat:example}
\end{figure}
\figurename~\ref{fig:combat:example} shows examples of combat.


\let\nextmark\mudmark
\subsection{Mud}
\label{sec:mud}

Turns~3 and~4 are \emph{mud} turns. All movement except Soviet rail
movement is reduced to 1 (clear or forest) hex per phase; rail
movement is unaffected. All units' \CF{}'s are halved when attacking
(not while defending).

For example, in the second example of combat in
\figref{fig:combat:example}, the three German units would be reduced
to a total \CF{}=8 versus the Soviet \CF{}=4, making the odds 2:1
(before terrain effects). The attacker's losses in an exchange are
based on the printed \CF{}, not halved \CF{}.

\let\nextmark\shockmark
\subsection{1\textsuperscript{st} Shock Army}

\begin{figure}
  \centering
  %% \includegraphics{03-01}
  \begin{tikzpicture}
    \chit[soviet 1S](0,0)
  \end{tikzpicture}
  \caption{1\textsuperscript{st} Shock Army}
\end{figure}
This unit may not begin on the map and may not be taken as a
replacement until turn 4 (Nov I/II).

\subsection{Replacements}
\label{sec:replacements}

A replacement is the ability to create a new half-strength unit (using
one which was previously eliminated), \emph{or} to flip an existing
half-strength unit to full strength. Both players get replacements
each turn, each in their respective replacement phases (phases 1 and
5). The German player gets one replacement, and the Soviet player gets
five.

A faction \emph{can not} use two replacements at once to create a new
full-strength unit. Creating a new full-strength unit from nothing
will take two turns of replacements.

New Soviet units appear on the east edge of the map (column 14$XX$),
in any empty hex, \emph{or} in any empty, friendly-owned city in
communication with the east edge (at most one per city). Existing
Soviet units, to be restored, \emph{must} also be in communication
with the east edge.

\emph{Friendly-owned} means that the faction units were the last units
in the city; all cities are owned by the Soviets at the beginning of
the game except for those that start occupied by a German unit.

\emph{In communication} means being able to trace a path, of any
length, not including the occupied hex, without entering a hex
containing an enemy unit or enemy ZOC, to the east edge of the map.
\figurename~\ref{fig:communication} illustrates this.

\emph{Exception}: the Soviets can bring in or restore a unit in Moscow
even if it isn't in communication.



\begin{figure}
  \centering
  \begin{tikzpicture}
    \draw[clip]
    (hex cs:c=10,r=8,e=SE,o=.5) rectangle(hex cs:c=13,r=5,e=SW,o=.5);

    \hexes

    \chit[soviet 16 flipped](1107)
    \chit[soviet 49](1206)
    
    \chit[german XIII](1106)
    \chit[german XLI](1108)
  \end{tikzpicture}
  \caption[In communication]{In communication. The Soviet
    16\textsuperscript{th} in Tula is \emph{not} in communication with
    the east edge of the board, since all possible supply lines would
    go through German ZOC.  The Soviet 49\textsuperscript{th} army
    \emph{is} in communication with the eastern edge of the board.}
  \label{fig:communication}
\end{figure}

German replacements work the same way, except that communication is
traced to the west edge and Moscow has no special properties. New
German units enter on the west edge of the map (column 01$XX$),
\emph{or} in a German controlled city. 

If replacements are not used, they may \emph{not} be saved for later
turns.

\paragraph*{Game Balance} 

If a handicap is needed for players of unequal experience, change
the replacements. To benefit the Germans, change the Soviet
replacements to four or even three. To benefit the Soviets, give the
Germans their replacement only on turns 2, 4, and 6.

\subsection{Starting the Game}
\label{sec:starting}

Set up one Soviet unit on each hex marked with red star
(\tikz[scale=.5]{\pic[scale=.5]{soviet star}}), all at half
strength. \emph{Do not} use the 1\textsuperscript{st} Shock Army (the
Soviet army with a combat strength of 10); it comes later. Since all
Soviet units are the same, it does not matter which unit goes
where. The Soviet faction should have four units left over (counting
the 1\textsuperscript{st} Shock Army); all except the
1\textsuperscript{st} Shock Army can be used as replacements in the
Soviet player's turn.

The German faction should then set up one German unit on each black
cross (\tikz[scale=.5]{\pic[scale=.5]{wehrmacht}}), all at full
strength. The exact setup is important since it helps to determine
what the German faction can do on turn~1.

After setting up the game, the German player begins at the combat
phase. Since all German units begin the game at full strength and
in-place, the German player receives no replacements on turn~1 and
skip the replacement and panzer movement phases.

\subsection{Winning the Game}
\label{sec:winning}

Whoever holds Moscow at the end of the game wins. A player holds
Moscow if one of his units was the last unit to be in the city. The
Soviets hold Moscow at the start of the game.


\onecolumn
%% Local Variables:
%% mode: LaTeX
%% TeX-master: "BattleForMoscow"
%% End:
